package com.example.workout_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.workout_app.DayActivities.DaysActivity
import com.example.workout_app.ExerciseActivities.ExercisesItem
import com.example.workout_app.ExerciseActivities.MuscleActivity
import com.example.workout_app.recordActivities.RecordExercisesActivity
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.muscle_row.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.File

class MainActivity : AppCompatActivity() {

    companion object {
        val adapter = GroupAdapter<ViewHolder>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter.clear()

        if (!File("/data/user/0/com.example.workout_app/files/exercises.json").exists()) {
            val text = ""
            val fos = openFileOutput("exercises.json", MODE_PRIVATE)
            fos.write(text.toByteArray())
        }

        MuscleActivity.adapter.clear()
        DaysActivity.adapter.clear()

        muscles_recyclerview.adapter = adapter
        muscles_recyclerview.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )

        adapter.add(MuscleItem("Shoulders"))
        adapter.add(MuscleItem("Triceps"))
        adapter.add(MuscleItem("Biceps"))
        adapter.add(MuscleItem("Chest"))
        adapter.add(MuscleItem("Six Pack"))
        adapter.add(MuscleItem("Legs"))
        adapter.add(MuscleItem("Back"))
        adapter.add(MuscleItem("Others"))

        adapter.setOnItemClickListener { item, view ->

            try {
                val it = item as MuscleItem
                val intent = Intent(this, MuscleActivity::class.java)
                intent.putExtra("Muscle", it.muscleName)
                startActivity(intent)
            } catch (e: ClassCastException) {
            }
        }

        ic_to_trains.setOnClickListener {
            val intent = Intent(this, DaysActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(intent)
        }

        ic_to_results.setOnClickListener {
            val intent = Intent(this, RecordExercisesActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(intent)
        }
    }
}

class MuscleItem(val muscleName: String) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.muscle_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.muscle_text.text = muscleName
        var shouldersExercises = 0
        var tricepsExercises = 0
        var bicepsExercises = 0
        var chestExercises = 0
        var sixPackExercises = 0
        var legsExercises = 0
        var backExercises = 0
        var otherExercises = 0

        viewHolder.itemView.ic_more.setOnClickListener {
            viewHolder.itemView.ic_more.visibility = View.GONE
            viewHolder.itemView.ic_less.visibility = View.VISIBLE

            File("/data/user/0/com.example.workout_app/files/exercises.json").forEachLine {
                val array = JSONArray(it)

                for (i in 0 until array.length()) {
                    val reader = JSONObject(array[i].toString())
                    val weight = reader.getString("weight")
                    val name = reader.getString("exerciseName")
                    val repetitions = reader.getString("repetitions")
                    val approaches = reader.getString("approaches")
                    val group = reader.getString("group")

                    if (muscleName == "Shoulders" && group == "Shoulders") {
                        MainActivity.adapter.add(
                            1 + shouldersExercises,
                            ExercisesItem(
                                name,
                                weight,
                                repetitions,
                                approaches,
                                muscleName
                            )
                        )
                        shouldersExercises += 1
                    }

                    if (muscleName == "Triceps" && group == "Triceps") {
                        MainActivity.adapter.add(
                            2 + tricepsExercises,
                            ExercisesItem(
                                name,
                                weight,
                                repetitions,
                                approaches,
                                muscleName
                            )
                        )
                        tricepsExercises += 1
                    }

                    if (muscleName == "Biceps" && group == "Biceps") {
                        MainActivity.adapter.add(
                            3 + bicepsExercises,
                            ExercisesItem(
                                name,
                                weight,
                                repetitions,
                                approaches,
                                muscleName
                            )
                        )
                        bicepsExercises += 1
                    }

                    if (muscleName == "Chest" && group == "Chest") {
                        MainActivity.adapter.add(
                            4 + chestExercises,
                            ExercisesItem(
                                name,
                                weight,
                                repetitions,
                                approaches,
                                muscleName
                            )
                        )
                        chestExercises += 1
                    }

                    if (muscleName == "Six Pack" && group == "Six Pack") {
                        MainActivity.adapter.add(
                            5 + sixPackExercises,
                            ExercisesItem(
                                name,
                                weight,
                                repetitions,
                                approaches,
                                muscleName
                            )
                        )
                        sixPackExercises += 1
                    }

                    if (muscleName == "Legs" && group == "Legs") {
                        MainActivity.adapter.add(
                            6 + legsExercises,
                            ExercisesItem(
                                name,
                                weight,
                                repetitions,
                                approaches,
                                muscleName
                            )
                        )
                        legsExercises += 1
                    }

                    if (muscleName == "Back" && group == "Back") {
                        MainActivity.adapter.add(
                            7 + backExercises,
                            ExercisesItem(
                                name,
                                weight,
                                repetitions,
                                approaches,
                                muscleName
                            )
                        )
                        backExercises += 1
                    }

                    if (muscleName == "Others" && group == "Others") {
                        MainActivity.adapter.add(
                            8 + otherExercises,
                            ExercisesItem(
                                name,
                                weight,
                                repetitions,
                                approaches,
                                muscleName
                            )
                        )
                        otherExercises += 1
                    }
                }
            }
        }

        viewHolder.itemView.ic_less.setOnClickListener {
            viewHolder.itemView.ic_less.visibility = View.GONE
            viewHolder.itemView.ic_more.visibility = View.VISIBLE

            File("/data/user/0/com.example.workout_app/files/exercises.json").forEachLine {
                val array = JSONArray(it)
                for (i in 0 until array.length()) {
                    val reader = JSONObject(array[i].toString())
                    val group = reader.getString("group")

                    if (muscleName == "Shoulders" && group == "Shoulders") {
                        MainActivity.adapter.removeGroup(0 + shouldersExercises)
                        shouldersExercises -= 1
                    }

                    if (muscleName == "Triceps" && group == "Triceps") {
                        MainActivity.adapter.removeGroup(1 + tricepsExercises)
                        tricepsExercises -= 1
                    }

                    if (muscleName == "Biceps" && group == "Biceps") {
                        MainActivity.adapter.removeGroup(2 + bicepsExercises)
                        bicepsExercises -= 1
                    }

                    if (muscleName == "Chest" && group == "Chest") {
                        MainActivity.adapter.removeGroup(3 + chestExercises)
                        chestExercises -= 1
                    }

                    if (muscleName == "Six Pack" && group == "Six Pack") {
                        MainActivity.adapter.removeGroup(4 + sixPackExercises)
                        sixPackExercises -= 1
                    }

                    if (muscleName == "Legs" && group == "Legs") {
                        MainActivity.adapter.removeGroup(5 + legsExercises)
                        legsExercises -= 1
                    }

                    if (muscleName == "Back" && group == "Back") {
                        MainActivity.adapter.removeGroup(6 + backExercises)
                        backExercises -= 1
                    }

                    if (muscleName == "Others" && group == "Others") {
                        MainActivity.adapter.removeGroup(6 + otherExercises)
                        otherExercises -= 1
                    }
                }
            }
        }
    }
}
