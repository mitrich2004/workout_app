package com.example.workout_app.DayActivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.workout_app.MainActivity
import com.example.workout_app.R
import com.example.workout_app.recordActivities.RecordExercisesActivity
import com.example.workout_app.TrainActivities.TrainsActivity
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_days.*
import kotlinx.android.synthetic.main.activity_days.ic_to_main
import kotlinx.android.synthetic.main.activity_days.ic_to_results
import kotlinx.android.synthetic.main.day_row.view.*
import org.json.JSONArray
import java.io.File

class DaysActivity : AppCompatActivity() {

    companion object {
        val adapter = GroupAdapter<ViewHolder>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_days)

        adapter.clear()

        if (!File("/data/user/0/com.example.workout_app/files/days.txt").exists()) {
            val text = arrayListOf(
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday",
                "Sunday"
            )
            val fos = openFileOutput("days.txt", MODE_PRIVATE)
            fos.write(text.toString().toByteArray())
        }

        adapter.setOnItemClickListener { item, view ->
            val dayItem = item as DaysItem
            val day = dayItem.day
            val intent = Intent(this, TrainsActivity::class.java)
            intent.putExtra("day", day)
            startActivity(intent)
        }

        days_recyclerview.adapter = adapter as GroupAdapter

        ic_to_main.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(intent)
        }

        ic_to_results.setOnClickListener {
            val intent = Intent(this, RecordExercisesActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(intent)
        }

        ic_settings.setOnClickListener {
            val intent = Intent(this, DaysSettingsActivity::class.java)
            startActivity(intent)
        }

        days_recyclerview.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )

        File("/data/user/0/com.example.workout_app/files/days.txt").forEachLine {
            val array = JSONArray(it)
            for (i in 0 until array.length()) {
                val day = array[i]
                adapter.add(
                    DaysItem(
                        day.toString()
                    )
                )
            }
        }
    }
}

class DaysItem(val day: String) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.day_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.day_text.text = day
    }
}
