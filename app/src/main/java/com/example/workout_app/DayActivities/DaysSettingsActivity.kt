package com.example.workout_app.DayActivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.workout_app.R
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_days_settings.*
import kotlinx.android.synthetic.main.days_settings_row.view.*
import org.json.JSONArray
import org.json.JSONException
import java.io.File

class DaysSettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_days_settings)

        ic_back.setOnClickListener {
            val intent = Intent(this, DaysActivity::class.java)
            startActivity(intent)
        }

        val adapter = GroupAdapter<ViewHolder>()

        days_recyclerview.adapter = adapter as GroupAdapter
        days_recyclerview.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )

        val daysArray = arrayListOf<String>()

        File("/data/user/0/com.example.workout_app/files/days.txt").forEachLine {
            val array = JSONArray(it)
            for (i in 0 until array.length()) {
                daysArray.add(array[i].toString())
            }

            if (daysArray.contains("Monday")) {
                adapter.add(DaysSettingsItem("Monday", true))
            } else {
                adapter.add(DaysSettingsItem("Monday", false))
            }

            if (daysArray.contains("Tuesday")) {
                adapter.add(DaysSettingsItem("Tuesday", true))
            } else {
                adapter.add(
                    DaysSettingsItem(
                        "Tuesday",
                        false
                    )
                )
            }

            if (daysArray.contains("Wednesday")) {
                adapter.add(
                    DaysSettingsItem(
                        "Wednesday",
                        true
                    )
                )
            } else {
                adapter.add(
                    DaysSettingsItem(
                        "Wednesday",
                        false
                    )
                )
            }

            if (daysArray.contains("Thursday")) {
                adapter.add(
                    DaysSettingsItem(
                        "Thursday",
                        true
                    )
                )
            } else {
                adapter.add(
                    DaysSettingsItem(
                        "Thursday",
                        false
                    )
                )
            }

            if (daysArray.contains("Friday")) {
                adapter.add(DaysSettingsItem("Friday", true))
            } else {
                adapter.add(DaysSettingsItem("Friday", false))
            }

            if (daysArray.contains("Saturday")) {
                adapter.add(
                    DaysSettingsItem(
                        "Saturday",
                        true
                    )
                )
            } else {
                adapter.add(
                    DaysSettingsItem(
                        "Saturday",
                        false
                    )
                )
            }

            if (daysArray.contains("Sunday")) {
                adapter.add(DaysSettingsItem("Sunday", true))
            } else {
                adapter.add(DaysSettingsItem("Sunday", false))
            }
        }
    }
}

class DaysSettingsItem(val day: String, var status: Boolean) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.days_settings_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        val array = arrayListOf<String>()

        viewHolder.itemView.day_text.text = day
        if (status) {
            viewHolder.itemView.ic_check.visibility = View.GONE
            viewHolder.itemView.ic_check_blue.visibility = View.VISIBLE
        } else {
            viewHolder.itemView.ic_check.visibility = View.VISIBLE
            viewHolder.itemView.ic_check_blue.visibility = View.GONE
        }

        viewHolder.itemView.ic_check.setOnClickListener {
            viewHolder.itemView.ic_check.visibility = View.GONE
            viewHolder.itemView.ic_check_blue.visibility = View.VISIBLE
            File("/data/user/0/com.example.workout_app/files/days.txt").forEachLine {
                if (it != "[]") {
                    array.add(it.replace("[", "").replace("]", ""))
                }
            }
            array.add(day)
            File("/data/user/0/com.example.workout_app/files/days.txt").writeText(array.toString())
            array.clear()
        }

        viewHolder.itemView.ic_check_blue.setOnClickListener {
            File("/data/user/0/com.example.workout_app/files/days.txt").forEachLine {
                val jsonArray = JSONArray(it)
                if (jsonArray.length() != 1) {
                    for (i in 0 until jsonArray.length()) {
                        try {
                            if (day == jsonArray[i].toString()) {
                                jsonArray.remove(i)
                            }
                        } catch (error: JSONException) {
                            jsonArray.remove(0)
                        }
                    }
                } else {
                    jsonArray.remove(0)
                }
                File("/data/user/0/com.example.workout_app/files/days.txt").writeText(jsonArray.toString())
            }
            viewHolder.itemView.ic_check.visibility = View.VISIBLE
            viewHolder.itemView.ic_check_blue.visibility = View.GONE
        }
    }

}
