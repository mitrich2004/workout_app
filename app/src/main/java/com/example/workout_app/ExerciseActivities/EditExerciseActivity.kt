package com.example.workout_app.ExerciseActivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.workout_app.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_add_exercise.approaches_number_edit
import kotlinx.android.synthetic.main.activity_add_exercise.exercise_name_edit
import kotlinx.android.synthetic.main.activity_add_exercise.ic_back
import kotlinx.android.synthetic.main.activity_add_exercise.repetitions_number_edit
import kotlinx.android.synthetic.main.activity_add_exercise.weight_edit
import kotlinx.android.synthetic.main.activity_edit_exercise.*
import java.io.File

class EditExerciseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_exercise)

        val weight = intent.getStringExtra("weight")
        val name = intent.getStringExtra("name")
        val repetitions = intent.getStringExtra("repetitions")
        val approaches = intent.getStringExtra("approaches")
        val muscleName = intent.getStringExtra("group")

        ic_back.setOnClickListener {
            val jsonArray = arrayListOf<Any>()

            File("/data/user/0/com.example.workout_app/files/exercises.json").forEachLine {
                if (jsonArray.toString() != "[]") {
                    jsonArray.add(it.replace("[", "").replace("]", ""))
                }

                jsonArray.add(
                    Gson().toJson(
                        ExercisesItem(
                            name!!, weight!!, repetitions!!, approaches!!, muscleName!!
                        )
                    )
                )
            }

            File("/data/user/0/com.example.workout_app/files/exercises.json").writeText(
                jsonArray.toString()
            )

            val intent = Intent(this, MuscleActivity::class.java)
            intent.putExtra("Muscle", muscleName)
            startActivity(intent)
        }

        exercise_name_edit.setText(name)
        weight_edit.setText(weight)
        repetitions_number_edit.setText(repetitions)
        approaches_number_edit.setText(approaches)

        val jsonArray = arrayListOf<Any>()

        File("/data/user/0/com.example.workout_app/files/exercises.json").forEachLine {
            if (jsonArray.toString() != "[]") {
                jsonArray.add(it.replace("[", "").replace("]", ""))
            }

        }
        ic_save_edit.setOnClickListener {
            if (exercise_name_edit.text.toString() != "" && weight_edit.text.toString() != "" && repetitions_number_edit.text.toString() != "" && approaches_number_edit.text.toString() != "") {
                jsonArray.add(
                    Gson().toJson(
                        ExercisesItem(
                            exercise_name_edit.text.toString(),
                            weight_edit.text.toString(),
                            repetitions_number_edit.text.toString(),
                            approaches_number_edit.text.toString(),
                            muscleName!!
                        )
                    )
                )

                File("/data/user/0/com.example.workout_app/files/exercises.json").writeText(
                    jsonArray.toString()
                )

                val intent = Intent(this, MuscleActivity::class.java)
                intent.putExtra("Muscle", muscleName)
                startActivity(intent)
            }
        }
    }
}
