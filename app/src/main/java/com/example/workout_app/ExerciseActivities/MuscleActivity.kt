package com.example.workout_app.ExerciseActivities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.workout_app.MainActivity
import com.example.workout_app.R
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_muscle.*
import kotlinx.android.synthetic.main.edit_row.view.*
import kotlinx.android.synthetic.main.exercise_row.view.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File

class MuscleActivity : AppCompatActivity() {

    companion object {
        val adapter = GroupAdapter<ViewHolder>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_muscle)

        adapter.clear()

        val muscleName = intent.getStringExtra("Muscle")
        muscle_text.text = muscleName

        ic_back.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

        File("$filesDir/exercises.json").forEachLine {
            val array = JSONArray(it)
            for (i in 0 until array.length()) {
                val reader = JSONObject(array[i].toString())
                val weight = reader.getString("weight")
                val name = reader.getString("exerciseName")
                val repetitions = reader.getString("repetitions")
                val approaches = reader.getString("approaches")
                val group = reader.getString("group")

                if (group == muscleName) {
                    adapter.add(
                        ExercisesItem(
                            name, weight,
                            repetitions, approaches, muscleName!!
                        )
                    )
                }
            }
        }

        ic_add.setOnClickListener {
            val intent = Intent(this, AddExerciseActivity::class.java)
            intent.putExtra("Muscle", muscleName)
            startActivity(intent)
        }

        exercises_recyclerview.adapter = adapter as GroupAdapter
        exercises_recyclerview.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )

        adapter.setOnItemLongClickListener { item, _ ->
            try {

                val index = adapter.getAdapterPosition(item)

                val it = item as ExercisesItem
                val itemWeight = it.weight
                val itemName = it.exerciseName
                val itemRepetitions = it.repetitions
                val itemApproaches = it.approaches
                val itemGroup = it.group

                adapter.add(
                    index + 1,
                    EditExerciseItem(
                        itemWeight,
                        itemName,
                        itemRepetitions,
                        itemApproaches,
                        itemGroup,
                        index,
                        this
                    )
                )
            } catch (e: ClassCastException) {
            }
            false
        }
    }
}

class ExercisesItem(
    val exerciseName: String,
    val weight: String,
    val repetitions: String,
    val approaches: String,
    val group: String
) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.exercise_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.exercise_name_text.text = exerciseName
        if (weight.toInt() != 0) {
            viewHolder.itemView.weight_text.text = weight + "kg"
        } else {
            viewHolder.itemView.weight_text.text = "Own Weight"
        }
        viewHolder.itemView.repetitions_approaches_text.text = repetitions + "/" + approaches
    }
}

class EditExerciseItem(
    private val itemWeight: String,
    private val itemName: String,
    private val itemRepetitions: String,
    private val itemApproaches: String,
    private val group: String,
    private val index: Int,
    private val context: Context
) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.edit_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.itemView.delete_text.setOnClickListener {
            File("/data/user/0/com.example.workout_app/files/exercises.json").forEachLine {
                val array = JSONArray(it)

                for (i in 0 until array.length()) {
                    try {
                        val reader = JSONObject(array[i].toString())
                        val weight = reader.getString("weight")
                        val name = reader.getString("exerciseName")
                        val repetitions = reader.getString("repetitions")
                        val approaches = reader.getString("approaches")
                        val muscleGroup = reader.getString("group")

                        if (itemWeight == weight && itemName == name && itemRepetitions == repetitions && itemApproaches == approaches && muscleGroup == group) {
                            array.remove(i)
                            File("/data/user/0/com.example.workout_app/files/exercises.json").writeText(
                                array.toString()
                            )
                        }
                    } catch (error: JSONException) {
                    }
                }
                MuscleActivity.adapter.removeGroup(index)
                MuscleActivity.adapter.removeGroup(index)
            }
        }

        viewHolder.itemView.edit_text.setOnClickListener {
            val itemGroup = group
            val intent = Intent(context, EditExerciseActivity::class.java)
            intent.putExtra("weight", itemWeight)
            intent.putExtra("name", itemName)
            intent.putExtra("repetitions", itemRepetitions)
            intent.putExtra("approaches", itemApproaches)
            intent.putExtra("group", itemGroup)

            File("/data/user/0/com.example.workout_app/files/exercises.json").forEachLine {
                val array = JSONArray(it)

                for (i in 0 until array.length()) {
                    try {
                        val reader = JSONObject(array[i].toString())
                        val weight = reader.getString("weight")
                        val name = reader.getString("exerciseName")
                        val repetitions = reader.getString("repetitions")
                        val approaches = reader.getString("approaches")
                        if (itemWeight == weight && itemName == name && itemRepetitions == repetitions && itemApproaches == approaches) {
                            array.remove(i)
                            MuscleActivity.adapter.removeGroup(index)
                            File("/data/user/0/com.example.workout_app/files/exercises.json").writeText(
                                array.toString()
                            )
                        }
                    } catch (error: JSONException) {

                    }
                }
            }
            MuscleActivity.adapter.removeGroup(index)
            startActivity(context, intent, null)
        }

        viewHolder.itemView.cancel_text.setOnClickListener {
            MuscleActivity.adapter.removeGroup(index + 1)
        }
    }
}
