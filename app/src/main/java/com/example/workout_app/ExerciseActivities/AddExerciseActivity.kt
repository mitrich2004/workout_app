package com.example.workout_app.ExerciseActivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.workout_app.MainActivity
import com.example.workout_app.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_add_exercise.*
import kotlinx.android.synthetic.main.activity_muscle.ic_back
import java.io.File

class AddExerciseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_exercise)

        MainActivity.adapter.clear()
        MuscleActivity.adapter.clear()

        val muscleName = intent.getStringExtra("Muscle")

        ic_back.setOnClickListener {
            val intent = Intent(this, MuscleActivity::class.java)
            intent.putExtra("Muscle", muscleName)
            startActivity(intent)
        }

        add_exercise_text.text = "Add ${muscleName.toLowerCase()} exercise"

        val jsonArray = arrayListOf<Any>()

        File("/data/user/0/com.example.workout_app/files/exercises.json").forEachLine {
            Log.d("TEST", it)
            if (it != "[]") {
                jsonArray.add(it.replace("[", "").replace("]", ""))
            }

        }
        ic_save.setOnClickListener {
            if (exercise_name_edit.text.toString() != "" && weight_edit.text.toString() != "" && repetitions_number_edit.text.toString() != "" && approaches_number_edit.text.toString() != "") {
                jsonArray.add(
                    Gson().toJson(
                        ExercisesItem(
                            exercise_name_edit.text.toString(),
                            weight_edit.text.toString(),
                            repetitions_number_edit.text.toString(),
                            approaches_number_edit.text.toString(),
                            muscleName!!
                        )
                    )
                )

                File("/data/user/0/com.example.workout_app/files/exercises.json").writeText(
                    jsonArray.toString()
                )

                val intent = Intent(this, MuscleActivity::class.java)
                intent.putExtra("Muscle", muscleName)
                startActivity(intent)
            }
        }
    }
}
