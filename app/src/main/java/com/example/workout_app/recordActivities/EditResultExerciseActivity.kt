package com.example.workout_app.recordActivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.workout_app.R
import com.example.workout_app.TrainActivities.TrainsActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_edit_result_exercise.*
import kotlinx.android.synthetic.main.activity_edit_train.*
import kotlinx.android.synthetic.main.activity_edit_train.toolbar_text
import java.io.File

class EditResultExerciseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_result_exercise)

        val name = intent.getStringExtra("name")

        edit_exercise_name_edittext.setText(name)

        ic_back.setOnClickListener {
            RecordExercisesActivity.adapter.clear()
            val jsonArray = arrayListOf<Any>()

            File("/data/user/0/com.example.workout_app/files/resultExercises.json").forEachLine {
                if (jsonArray.toString() != "[]") {
                    jsonArray.add(it.replace("[", "").replace("]", ""))
                }

                jsonArray.add(
                    Gson().toJson(
                        RecordExerciseItem(
                            name!!
                        )
                    ))
            }

            File("/data/user/0/com.example.workout_app/files/resultExercises.json").writeText(
                jsonArray.toString()
            )

            val intent = Intent(this, RecordExercisesActivity::class.java)
            startActivity(intent)
        }

        val jsonArray = arrayListOf<Any>()

        File("/data/user/0/com.example.workout_app/files/resultExercises.json").forEachLine {
            if (jsonArray.toString() != "[]") {
                jsonArray.add(it.replace("[", "").replace("]", ""))
            }

        }

        ic_save.setOnClickListener {
            if (edit_exercise_name_edittext.text.toString() != "") {
                jsonArray.add(
                    Gson().toJson(
                        RecordExerciseItem(
                            edit_exercise_name_edittext.text.toString()
                        )
                    )
                )

                File("/data/user/0/com.example.workout_app/files/resultExercises.json").writeText(
                    jsonArray.toString()
                )

                val intent = Intent(this, RecordExercisesActivity::class.java)
                startActivity(intent)
            }
        }
    }
}
