package com.example.workout_app.recordActivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.workout_app.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_add_result_exercise.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class AddResultActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_result)

        val name = intent.getStringExtra("name")

        ic_back.setOnClickListener {
            val intent = Intent(this, ResultsActivity::class.java)
            intent.putExtra("name", name)
            startActivity(intent)
        }

        val jsonArray = arrayListOf<Any>()

        File("$filesDir/$name.json").forEachLine {
            Log.d("TEST", it)
            if (it != "[]") {
                jsonArray.add(it.replace("[", "").replace("]", ""))
            }
        }

        val format = SimpleDateFormat("dd.MM.yyyy")
        val currentDate = format.format(Date())

        ic_save.setOnClickListener {
            if (exercise_name_edittext.text.toString() != "") {
                jsonArray.add(
                    Gson().toJson(
                        ResultItem(
                            currentDate,
                            exercise_name_edittext.text.toString().toInt()
                        )
                    )
                )

                File("$filesDir/$name.json").writeText(
                    jsonArray.toString()
                )

                val intent = Intent(this, ResultsActivity::class.java)
                intent.putExtra("name", name)
                startActivity(intent)
            }
        }
    }
}
