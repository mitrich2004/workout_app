package com.example.workout_app.recordActivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.workout_app.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_add_result_exercise.*
import kotlinx.android.synthetic.main.activity_add_result_exercise.ic_back
import kotlinx.android.synthetic.main.activity_add_result_exercise.ic_save
import java.io.File

class AddResultExerciseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_result_exercise)

        ic_back.setOnClickListener {
            val intent = Intent(this, RecordExercisesActivity::class.java)
            startActivity(intent)
        }

        val jsonArray = arrayListOf<Any>()

        File("$filesDir/resultExercises.json").forEachLine {
            Log.d("TEST", it)
            if (it != "[]") {
                jsonArray.add(it.replace("[", "").replace("]", ""))
            }
        }

        ic_save.setOnClickListener {
            if (exercise_name_edittext.text.toString() != "") {
                jsonArray.add(
                    Gson().toJson(
                        RecordExerciseItem(
                            exercise_name_edittext.text.toString()
                        )
                    )
                )

                File("$filesDir/resultExercises.json").writeText(
                    jsonArray.toString()
                )

                val intent = Intent(this, RecordExercisesActivity::class.java)
                startActivity(intent)
            }
        }
    }
}
