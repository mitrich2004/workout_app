package com.example.workout_app.recordActivities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.workout_app.MainActivity
import com.example.workout_app.R
import com.example.workout_app.DayActivities.DaysActivity
import com.example.workout_app.TrainActivities.EditTrainActivity
import com.example.workout_app.TrainActivities.EditTrainItem
import com.example.workout_app.TrainActivities.TrainItem
import com.example.workout_app.TrainActivities.TrainsActivity
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_record_exercises.*
import kotlinx.android.synthetic.main.edit_row.view.*
import kotlinx.android.synthetic.main.record_exercise_row.view.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File

class RecordExercisesActivity : AppCompatActivity() {

    companion object {
        val adapter = GroupAdapter<ViewHolder>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record_exercises)

        adapter.clear()

        if (!File("$filesDir/resultExercises.json").exists()) {
            val text = ""
            val fos = openFileOutput("resultExercises.json", MODE_PRIVATE)
            fos.write(text.toByteArray())
        }

        ic_add.setOnClickListener {
            val intent = Intent(this, AddResultExerciseActivity::class.java)
            startActivity(intent)
            adapter.clear()
        }

        ic_to_trains.setOnClickListener {
            val intent = Intent(this, DaysActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(intent)
        }

        record_exercises_recyclerview.adapter = adapter as GroupAdapter

        record_exercises_recyclerview.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )

        ic_to_main.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(intent)
        }

        File("$filesDir/resultExercises.json").forEachLine {
            val array = JSONArray(it)
            for (i in 0 until array.length()) {
                val reader = JSONObject(array[i].toString())
                val name = reader.getString("name")

                adapter.add(RecordExerciseItem(name))
            }
        }

        adapter.setOnItemLongClickListener { item, _ ->

            val index = adapter.getAdapterPosition(item)

            val it = item as RecordExerciseItem
            val name = it.name

            adapter.add(
                index + 1,
                EditRecordExerciseItem(index, this, name)
            )
            true
        }

        adapter.setOnItemClickListener { item, _ ->
            val it = item as RecordExerciseItem
            val name = it.name
            val intent = Intent(this, ResultsActivity::class.java)
            intent.putExtra("name", name)
            startActivity(intent)
        }
    }
}

class RecordExerciseItem(val name: String) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.record_exercise_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.record_exercise_name.text = name
    }

}

class EditRecordExerciseItem(val index: Int, val context: Context, val name: String): Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.edit_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.cancel_text.setOnClickListener {
            RecordExercisesActivity.adapter.removeGroup(index + 1)
        }

        viewHolder.itemView.edit_text.setOnClickListener {
            val intent = Intent(context, EditResultExerciseActivity::class.java)
            intent.putExtra("name", name)

            File("/data/user/0/com.example.workout_app/files/resultExercises.json").forEachLine {
                val array = JSONArray(it)

                for (i in 0 until array.length()) {
                    try {
                        val reader = JSONObject(array[i].toString())
                        val itemName = reader.getString("name")

                        if (itemName == name) {
                            array.remove(i)
                            RecordExercisesActivity.adapter.removeGroup(index)
                            File("/data/user/0/com.example.workout_app/files/resultExercises.json").writeText(
                                array.toString()
                            )
                        }
                    } catch (error: JSONException) {
                    }
                }
            }
            RecordExercisesActivity.adapter.removeGroup(index)
            ContextCompat.startActivity(context, intent, null)
        }

        viewHolder.itemView.delete_text.setOnClickListener {
            File("/data/user/0/com.example.workout_app/files/resultExercises.json").forEachLine {
                val array = JSONArray(it)

                for (i in 0 until array.length()) {
                    try {
                        val reader = JSONObject(array[i].toString())
                        val itemName = reader.getString("name")

                        if (itemName == name) {
                            array.remove(i)
                            File("/data/user/0/com.example.workout_app/files/resultExercises.json").writeText(
                                array.toString()
                            )
                        }
                    } catch (error: JSONException) {
                    }
                }
                RecordExercisesActivity.adapter.removeGroup(index)
                RecordExercisesActivity.adapter.removeGroup(index)
            }
        }
    }

}