package com.example.workout_app.recordActivities

import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.workout_app.R
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_results.*
import kotlinx.android.synthetic.main.edit_column.view.*
import kotlinx.android.synthetic.main.edit_column.view.cancel_text
import kotlinx.android.synthetic.main.edit_column.view.delete_text
import kotlinx.android.synthetic.main.edit_row.view.*
import kotlinx.android.synthetic.main.result_column.view.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.text.Format
import java.text.SimpleDateFormat
import java.util.*

class ResultsActivity : AppCompatActivity() {

    companion object {
        val adapter = GroupAdapter<ViewHolder>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_results)

        adapter.clear()

        val name = intent.getStringExtra("name")

        if (!File("$filesDir/$name.json").exists()) {
            val text = ""
            val fos = openFileOutput("$name.json", MODE_PRIVATE)
            fos.write(text.toByteArray())
        }


        ic_back.setOnClickListener {
            val intent = Intent(this, RecordExercisesActivity::class.java)
            startActivity(intent)
        }

        toolbar_text.text = "$name Results"

        results_recyclerview.adapter = adapter
        results_recyclerview.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.HORIZONTAL
            )
        )

        val lineChart = findViewById<LineChart>(R.id.line_chart)

        val yAXES = ArrayList<Entry>()
        val xAXES = ArrayList<String>()

        File("$filesDir/$name.json").forEachLine {
            val array = JSONArray(it)
            for (i in 0 until array.length()) {
                val reader = JSONObject(array[i].toString())
                val result = reader.getInt("result")
                val date = reader.getString("date")

                adapter.add(ResultItem(date, result))
                yAXES.add(Entry(result.toFloat(), i))
                xAXES.add(i, date.substring(0, 5))
            }
        }

        val xaxes = arrayOfNulls<String>(xAXES.size)
        for (i in 0 until xAXES.size) {
            xaxes[i] = xAXES[i]
        }

        val lineDataSets = ArrayList<ILineDataSet>()

        val lineDataSet = LineDataSet(yAXES, "Result")
        lineDataSet.setDrawCircles(false)
        lineDataSet.color = Color.BLUE
        lineDataSet.setDrawCubic(true)
        lineDataSet.setDrawCircles(true)
        lineDataSet.setCircleColor(Color.BLUE)
        lineDataSet.setCircleColorHole(Color.BLUE)

        lineDataSets.add(lineDataSet)
        lineChart.data = LineData(xaxes, lineDataSets)
        lineChart.setVisibleXRangeMaximum(65f)
        lineChart.isDoubleTapToZoomEnabled = false
        lineChart.setDrawBorders(true)

        adapter.add(AddResultItem(name!!))

        results_recyclerview.scrollToPosition(adapter.itemCount)

        adapter.setOnItemClickListener { item, _ ->
            try {
                val it = item as AddResultItem
                val name = it.name
                val intent = Intent(this, AddResultActivity::class.java)
                intent.putExtra("name", name)
                startActivity(intent)
            } catch (error: ClassCastException) {

            }
        }

        adapter.setOnItemLongClickListener { item, _ ->
            try {
                val it = item as ResultItem
                val index = adapter.getAdapterPosition(it)
                Log.d("INDEX", index.toString())

                adapter.remove(item)
                adapter.add(index, EditResultItem(index, this, it.result.toString(), name, it.date))
            } catch (error: ClassCastException) {

            }
            true
        }
    }
}

class ResultItem(val date: String, val result: Int) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.result_column
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.date_text.text = date
        viewHolder.itemView.result_text.text = result.toString()
    }
}

class AddResultItem(val name: String) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.add_result_column
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {

    }

}

class EditResultItem(
    val index: Int,
    val context: Context,
    val result: String,
    val name: String,
    val date: String
) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.edit_column
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.cancel_text.setOnClickListener {
            File("/data/user/0/com.example.workout_app/files/$name.json").forEachLine {
                ResultsActivity.adapter.removeGroup(index)
                val array = JSONArray(it)

                val reader = JSONObject(array[index].toString())
                val result = reader.getInt("result")
                val date = reader.getString("date")

                ResultsActivity.adapter.add(index, ResultItem(date, result))
            }

        }
    }
}
