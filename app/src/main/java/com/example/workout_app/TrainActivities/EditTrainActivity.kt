package com.example.workout_app.TrainActivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.workout_app.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_edit_train.*
import kotlinx.android.synthetic.main.activity_edit_train.toolbar_text
import java.io.File

class EditTrainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_train)

        val day = intent.getStringExtra("day")
        val name = intent.getStringExtra("name")
        val time = intent.getStringExtra("time")

        ic_back_edit.setOnClickListener {
            val jsonArray = arrayListOf<Any>()

            File("/data/user/0/com.example.workout_app/files/$day.json").forEachLine {
                if (jsonArray.toString() != "[]") {
                    jsonArray.add(it.replace("[", "").replace("]", ""))
                }

                jsonArray.add(Gson().toJson(
                    TrainItem(
                        name!!,
                        time!!
                    )
                ))
            }

            File("/data/user/0/com.example.workout_app/files/$day.json").writeText(
                jsonArray.toString()
            )

            val intent = Intent(this, TrainsActivity::class.java)
            intent.putExtra("day", day)
            startActivity(intent)
        }

        toolbar_text.text = "Edit \"$name\""
        train_name_edit.setText(name)
        train_time_edit.setText(time)

        val jsonArray = arrayListOf<Any>()

        File("/data/user/0/com.example.workout_app/files/$day.json").forEachLine {
            if (jsonArray.toString() != "[]") {
                jsonArray.add(it.replace("[", "").replace("]", ""))
            }

        }

        ic_save_edit_train.setOnClickListener {
            if (train_name_edit.text.toString() != "" && train_time_edit.text.toString() != "") {
                jsonArray.add(
                    Gson().toJson(
                        TrainItem(
                            train_name_edit.text.toString(),
                            train_time_edit.text.toString()
                        )
                    )
                )

                File("/data/user/0/com.example.workout_app/files/$day.json").writeText(
                    jsonArray.toString()
                )

                val intent = Intent(this, TrainsActivity::class.java)
                intent.putExtra("day", day)
                startActivity(intent)
            }
        }
    }
}
