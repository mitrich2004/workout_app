package com.example.workout_app.TrainActivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.workout_app.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_add_exercise.ic_save
import kotlinx.android.synthetic.main.activity_add_train.*
import kotlinx.android.synthetic.main.activity_trains.ic_back
import java.io.File

class AddTrainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_train)

        TrainsActivity.adapter.clear()

        val day = intent.getStringExtra("day")

        ic_back.setOnClickListener {
            val intent = Intent(this, TrainsActivity::class.java)
            intent.putExtra("day", day)
            startActivity(intent)
        }

        val jsonArray = arrayListOf<Any>()

        File("/data/user/0/com.example.workout_app/files/$day.json").forEachLine {
            Log.d("TEST", it)
            if (it != "[]") {
                jsonArray.add(it.replace("[", "").replace("]", ""))
            }
        }

        ic_save.setOnClickListener {
            if (train_name_edittext.text.toString() != "" && time_edittext.text.toString() != "") {
                jsonArray.add(
                    Gson().toJson(
                        TrainItem(
                            train_name_edittext.text.toString(),
                            time_edittext.text.toString()
                        )
                    )
                )

                File("/data/user/0/com.example.workout_app/files/$day.json").writeText(
                    jsonArray.toString()
                )

                val intent = Intent(this, TrainsActivity::class.java)
                intent.putExtra("day", day)
                startActivity(intent)
            }
        }
    }
}
