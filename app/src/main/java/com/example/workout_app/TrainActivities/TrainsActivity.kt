package com.example.workout_app.TrainActivities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.workout_app.DayActivities.DaysActivity
import com.example.workout_app.R
import com.example.workout_app.trainExercisesActivities.TrainExercisesActivity
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_trains.*
import kotlinx.android.synthetic.main.edit_row.view.*
import kotlinx.android.synthetic.main.edit_row.view.cancel_text
import kotlinx.android.synthetic.main.edit_row.view.delete_text
import kotlinx.android.synthetic.main.train_row.view.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File

class TrainsActivity : AppCompatActivity() {

    companion object {
        val adapter = GroupAdapter<ViewHolder>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trains)

        adapter.clear()

        val day = intent.getStringExtra("day")

        toolbar_text.text = day

        if (!File("/data/user/0/com.example.workout_app/files/$day.json").exists()) {
            val text = ""
            val fos = openFileOutput("$day.json", MODE_PRIVATE)
            fos.write(text.toByteArray())
        }

        ic_back.setOnClickListener {
            val intent =  Intent(this, DaysActivity::class.java)
            intent.putExtra("day", day)
            startActivity(intent)
        }

        ic_add.setOnClickListener {
            val intent = Intent(this, AddTrainActivity::class.java)
            intent.putExtra("day", day)
            startActivity(intent)
        }

        trains_recyclerview.adapter = adapter as GroupAdapter
        trains_recyclerview.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )

        File("$filesDir/$day.json").forEachLine {
            val array = JSONArray(it)
            for (i in 0 until array.length()) {
                val reader = JSONObject(array[i].toString())
                val time = reader.getString("time")
                val name = reader.getString("name")

                adapter.add(
                    TrainItem(
                        name,
                        time
                    )
                )
            }
        }

        adapter.setOnItemClickListener { item, _ ->
            try {
                val it = item as TrainItem
                val intent = Intent(this, TrainExercisesActivity::class.java)
                intent.putExtra("name", it.name)
                intent.putExtra("day", day)
                startActivity(intent)
            } catch (e: ClassCastException) {
            }
        }

        adapter.setOnItemLongClickListener { item, _ ->

            val index = adapter.getAdapterPosition(item)

            val it = item as TrainItem
            val name = it.name
            val time = it.time

            adapter.add(
                index + 1,
                EditTrainItem(index, this, name, time, day!!)
            )
            true
        }
    }
}

class TrainItem(val name: String, val time: String) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.train_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.time_text.text = time + "min"
        viewHolder.itemView.train_name.text = name
    }

}

class EditTrainItem(
    private val index: Int,
    val context: Context,
    val name: String,
    private val time: String,
    private val day: String
) :
    Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.edit_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.cancel_text.setOnClickListener {
            TrainsActivity.adapter.removeGroup(index + 1)
        }

        viewHolder.itemView.edit_text.setOnClickListener {
            val intent = Intent(context, EditTrainActivity::class.java)
            intent.putExtra("time", time)
            intent.putExtra("name", name)
            intent.putExtra("day", day)

            File("/data/user/0/com.example.workout_app/files/$day.json").forEachLine {
                val array = JSONArray(it)

                for (i in 0 until array.length()) {
                    try {
                        val reader = JSONObject(array[i].toString())
                        val itemTime = reader.getString("time")
                        val itemName = reader.getString("name")

                        if (itemTime == time && itemName == name) {
                            array.remove(i)
                            TrainsActivity.adapter.removeGroup(index)
                            File("/data/user/0/com.example.workout_app/files/$day.json").writeText(
                                array.toString()
                            )
                        }
                    } catch (error: JSONException) {

                    }
                }
            }
            TrainsActivity.adapter.removeGroup(index)
            ContextCompat.startActivity(context, intent, null)
        }

        viewHolder.itemView.delete_text.setOnClickListener {
            File("/data/user/0/com.example.workout_app/files/$day.json").forEachLine {
                val array = JSONArray(it)

                for (i in 0 until array.length()) {
                    try {
                        val reader = JSONObject(array[i].toString())
                        val itemTime = reader.getString("time")
                        val itemName = reader.getString("name")

                        if (itemName == name && itemTime == time) {
                            array.remove(i)
                            File("/data/user/0/com.example.workout_app/files/$day.json").writeText(
                                array.toString()
                            )
                        }
                    } catch (error: JSONException) {
                    }
                }
                TrainsActivity.adapter.removeGroup(index)
                TrainsActivity.adapter.removeGroup(index)
            }
        }
    }
}

