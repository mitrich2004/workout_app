package com.example.workout_app.trainExercisesActivities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.workout_app.R
import com.example.workout_app.TrainActivities.TrainsActivity
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_train_exercises.*
import kotlinx.android.synthetic.main.edit_row.view.*
import kotlinx.android.synthetic.main.exercise_row.view.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File

class TrainExercisesActivity : AppCompatActivity() {

    companion object {
        val adapter = GroupAdapter<ViewHolder>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_train_exercises)

        adapter.clear()

        if (!File("/data/user/0/com.example.workout_app/files/trainsExercises.json").exists()) {
            val text = ""
            val fos = openFileOutput("trainsExercises.json", MODE_PRIVATE)
            fos.write(text.toByteArray())
        }

        train_exercises_recyclerview.adapter = adapter as GroupAdapter

        train_exercises_recyclerview.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )

        val trainName = intent.getStringExtra("name")
        val day = intent.getStringExtra("day")

        ic_back.setOnClickListener {
            val intent = Intent(this, TrainsActivity::class.java)
            intent.putExtra("day", day)
            intent.putExtra("name", trainName)
            startActivity(intent)
        }

        train_text.text = trainName

        ic_add_train_exercise.setOnClickListener {
            val intent = Intent(this, AddTrainExerciseActivity::class.java)
            intent.putExtra("name", trainName)
            startActivity(intent)
        }


        File("$filesDir/trainsExercises.json").forEachLine {
            val array = JSONArray(it)
            for (i in 0 until array.length()) {
                val reader = JSONObject(array[i].toString())
                val weight = reader.getString("weight")
                val name = reader.getString("name")
                val repetitions = reader.getString("repetitions")
                val approaches = reader.getString("approaches")
                val group = reader.getString("trainName")

                if (group == trainName) {
                    adapter.add(
                        TrainExerciseItem(
                            name, weight,
                            repetitions, approaches, trainName!!
                        )
                    )
                }
            }
        }

        adapter.setOnItemLongClickListener { item, view ->
            try {
                val index = adapter.getAdapterPosition(item)

                val it = item as TrainExerciseItem
                val itemWeight = it.weight
                val itemName = it.name
                val itemRepetitions = it.repetitions
                val itemApproaches = it.approaches
                val itemGroup = it.trainName

                adapter.add(
                    index + 1,
                    EditTrainExerciseItem(
                        itemWeight,
                        itemName,
                        itemRepetitions,
                        itemApproaches,
                        itemGroup,
                        index,
                        this
                    )
                )
            } catch (e: ClassCastException) {
            }
            false
        }
    }
}

class TrainExerciseItem(
    val name: String,
    val repetitions: String,
    val approaches: String,
    val weight: String,
    val trainName: String
) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.exercise_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.exercise_name_text.text = name
        viewHolder.itemView.repetitions_approaches_text.text = repetitions + "/" + approaches
        if (weight.toInt() != 0) {
            viewHolder.itemView.weight_text.text = weight + "kg"
        } else {
            viewHolder.itemView.weight_text.text = "Own Weight"
        }
    }
}

class EditTrainExerciseItem(
    val itemApproaches: String,
    val itemName: String,
    val itemWeight: String,
    val itemRepetitions: String,
    val trainName: String,
    val index: Int,
    val context: Context
) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.edit_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.cancel_text.setOnClickListener {
            TrainExercisesActivity.adapter.removeGroup(index + 1)
        }

        viewHolder.itemView.delete_text.setOnClickListener {
            File("/data/user/0/com.example.workout_app/files/trainsExercises.json").forEachLine {
                val array = JSONArray(it)

                for (i in 0 until array.length()) {
                    try {
                        val reader = JSONObject(array[i].toString())
                        val weight = reader.getString("weight")
                        val name = reader.getString("name")
                        val repetitions = reader.getString("repetitions")
                        val approaches = reader.getString("approaches")
                        val train = reader.getString("trainName")

                        if (itemWeight == weight && itemName == name && itemRepetitions == repetitions && itemApproaches == approaches && trainName == train) {
                            array.remove(i)
                            File("/data/user/0/com.example.workout_app/files/trainsExercises.json").writeText(
                                array.toString()
                            )
                        }
                    } catch (error: JSONException) {
                    }
                }
                TrainExercisesActivity.adapter.removeGroup(index)
                TrainExercisesActivity.adapter.removeGroup(index)
            }
        }

        viewHolder.itemView.edit_text.setOnClickListener {
            val intent = Intent(context, EditTrainExercisesActivity::class.java)
            intent.putExtra("weight", itemWeight)
            intent.putExtra("name", itemName)
            intent.putExtra("repetitions", itemRepetitions)
            intent.putExtra("approaches", itemApproaches)
            intent.putExtra("trainName", trainName)

            File("/data/user/0/com.example.workout_app/files/trainsExercises.json").forEachLine {
                val array = JSONArray(it)

                for (i in 0 until array.length()) {
                    try {
                        val reader = JSONObject(array[i].toString())
                        val weight = reader.getString("weight")
                        val name = reader.getString("exerciseName")
                        val repetitions = reader.getString("repetitions")
                        val approaches = reader.getString("approaches")
                        if (itemWeight == weight && itemName == name && itemRepetitions == repetitions && itemApproaches == approaches) {
                            array.remove(i)
                            File("/data/user/0/com.example.workout_app/files/trainsExercises.json").writeText(
                                array.toString()
                            )
                        }
                    } catch (error: JSONException) {

                    }
                }
            }
            TrainExercisesActivity.adapter.removeGroup(index)
            TrainExercisesActivity.adapter.removeGroup(index)
            ContextCompat.startActivity(context, intent, null)
        }
    }
}
