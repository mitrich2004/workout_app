package com.example.workout_app.trainExercisesActivities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.workout_app.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_add_exercise.approaches_number_edit
import kotlinx.android.synthetic.main.activity_add_exercise.exercise_name_edit
import kotlinx.android.synthetic.main.activity_add_exercise.ic_back
import kotlinx.android.synthetic.main.activity_add_exercise.repetitions_number_edit
import kotlinx.android.synthetic.main.activity_add_exercise.weight_edit
import kotlinx.android.synthetic.main.activity_edit_exercise.*
import java.io.File

class EditTrainExercisesActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_exercise)

        val weight = intent.getStringExtra("weight")
        val name = intent.getStringExtra("name")
        val repetitions = intent.getStringExtra("repetitions")
        val approaches = intent.getStringExtra("approaches")
        val trainName = intent.getStringExtra("trainName")

        ic_back.setOnClickListener {
            val jsonArray = arrayListOf<Any>()

            File("/data/user/0/com.example.workout_app/files/trainsExercises.json").forEachLine {
                if (jsonArray.toString() != "[]") {
                    jsonArray.add(it.replace("[", "").replace("]", ""))
                }

                jsonArray.add(
                    Gson().toJson(
                        TrainExerciseItem(
                            name!!, weight!!, repetitions!!, approaches!!, trainName!!
                        )
                    )
                )
            }

            File("/data/user/0/com.example.workout_app/files/trainsExercises.json").writeText(
                jsonArray.toString()
            )

            val intent = Intent(this, TrainExercisesActivity::class.java)
            intent.putExtra("name", trainName)
            Log.d("TEST", trainName)
            startActivity(intent)
        }

        exercise_name_edit.setText(name)
        weight_edit.setText(weight)
        repetitions_number_edit.setText(repetitions)
        approaches_number_edit.setText(approaches)

        val jsonArray = arrayListOf<Any>()

        File("/data/user/0/com.example.workout_app/files/trainsExercises.json").forEachLine {
            if (jsonArray.toString() != "[]") {
                jsonArray.add(it.replace("[", "").replace("]", ""))
            }

        }
        ic_save_edit.setOnClickListener {
            if (exercise_name_edit.text.toString() != "" && weight_edit.text.toString() != "" && repetitions_number_edit.text.toString() != "" && approaches_number_edit.text.toString() != "") {
                jsonArray.add(
                    Gson().toJson(
                        TrainExerciseItem(
                            exercise_name_edit.text.toString(),
                            weight_edit.text.toString(),
                            repetitions_number_edit.text.toString(),
                            approaches_number_edit.text.toString(),
                            trainName!!
                        )
                    )
                )

                File("/data/user/0/com.example.workout_app/files/trainsExercises.json").writeText(
                    jsonArray.toString()
                )

                val intent = Intent(this, TrainExercisesActivity::class.java)
                intent.putExtra("name", trainName)
                startActivity(intent)
            }
        }
    }
}