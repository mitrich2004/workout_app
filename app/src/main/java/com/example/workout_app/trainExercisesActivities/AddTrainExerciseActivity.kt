package com.example.workout_app.trainExercisesActivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.workout_app.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_add_exercise.*
import kotlinx.android.synthetic.main.activity_muscle.ic_back
import java.io.File

class AddTrainExerciseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_exercise)

        val trainName = intent.getStringExtra("name")

        ic_back.setOnClickListener {
            val intent = Intent(this, TrainExercisesActivity::class.java)
            intent.putExtra("name", trainName)
            startActivity(intent)
        }

        val jsonArray = arrayListOf<Any>()

        File("/data/user/0/com.example.workout_app/files/trainsExercises.json").forEachLine {
            if (it != "[]") {
                jsonArray.add(it.replace("[", "").replace("]", ""))
            }
        }

        ic_save.setOnClickListener {
            if (exercise_name_edit.text.toString() != "" && weight_edit.text.toString() != "" && repetitions_number_edit.text.toString() != "" && approaches_number_edit.text.toString() != "") {

                jsonArray.add(
                    Gson().toJson(
                        TrainExerciseItem(
                            exercise_name_edit.text.toString(),
                            weight_edit.text.toString(),
                            repetitions_number_edit.text.toString(),
                            approaches_number_edit.text.toString(),
                            trainName!!
                        )
                    )
                )
            }

            File("/data/user/0/com.example.workout_app/files/trainsExercises.json").writeText(
                jsonArray.toString()
            )

            TrainExercisesActivity.adapter.clear()
            val intent = Intent(this, TrainExercisesActivity::class.java)
            intent.putExtra("name", trainName)
            startActivity(intent)
        }
    }
}
